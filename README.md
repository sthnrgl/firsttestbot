# RT bot  
This is telegram bot to help radiotherapists and medical physicists.
Contact and feedback: *[b.botvinovsky@gmail.com](b.botvinovsky@gmail.com)*

### Console args or file "logins" in root folder project:

     1) main bot name
     2) main bot token
     3) second log bot name
     4) second log bot token
     5) superuser telegram id
     6) login to bd
     7) pass to bd
     8) pass for login as admin (in main menu)

Thanks for the support JetBrains: [jb.gg/OpenSourceSupport](https://jb.gg/OpenSourceSupport)  
![jet brains logo](jb_beam.png)
 

