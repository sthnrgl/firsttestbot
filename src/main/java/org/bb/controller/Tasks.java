package org.bb.controller;

import org.bb.App;
import org.bb.Login;
import org.bb.db.DB;
import org.bb.settings.Settings;
import org.bb.user.ActiveUsersMap;
import org.bb.user.User;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class Tasks {
    public void runBotStatus(){
        Runnable task = () -> {
            while (true) {
                try {
                    this.botStatus();
                } catch (InterruptedException e) {
                    System.out.println("runnable task failed");
                }
            }
        };
        Thread thread = new Thread(task);
        thread.start();
    }
    private void botStatus() throws InterruptedException {
        TimeUnit.MINUTES.sleep(Settings.sleepScheduledTasks());
        User user = DB.getUser(Login.getSuperUserID());
        ArrayList<Message> messages = new ArrayList<>();
        String s = String.format("""
                Status:
                %s
                Active users: %s
                Users in DB: %s
                """,
                new Timestamp(System.currentTimeMillis()),
                ActiveUsersMap.getNumberOfActiveUsers(),
                DB.numberOfUsers()
        );

        messages.add(new Message(user, s));
        App.logbot.send(messages);
    }

    public String onStart(){
        return String.format("""
                        Time: %s
                        Bot %s started
                        Bot %s started
                        DB: %s users
                        """,
                new Timestamp(System.currentTimeMillis()),
                Login.getBotName1(),
                Login.getBotName2(),
                DB.numberOfUsers()
                );
    }
}
