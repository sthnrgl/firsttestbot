package org.bb.controller.activities;

import org.bb.controller.Message;

import java.util.ArrayList;

public interface IActivity {
    ArrayList<Message> run(Message m);
}
