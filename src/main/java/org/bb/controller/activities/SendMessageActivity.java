package org.bb.controller.activities;

import org.bb.db.DB;
import org.bb.controller.Message;
import org.bb.user.User;
import java.util.ArrayList;

public class SendMessageActivity implements IActivity {
    @Override
    public ArrayList<Message> run(Message m) {
        ArrayList<Message> messages = new ArrayList<>();
        String text = m.text;
        int i = text.indexOf(' ');
        String recipientId = text.substring(0, i);
        String sendedText = text.substring(i);
        if (DB.isUserExist(recipientId)){
            User recipient = DB.getUser(recipientId);
            messages.add(new Message(recipient, sendedText));
            assert recipient != null;
            messages.add(new Message(m.user, "sended to " + recipient.firstName));
            return messages;
        } else {
            messages.add(new Message(m.user, "unknown user"));
            return messages;
        }
    }
}
