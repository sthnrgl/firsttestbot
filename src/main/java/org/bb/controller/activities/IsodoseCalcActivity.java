package org.bb.controller.activities;

import org.bb.controller.Message;
import org.bb.apps.IsodoseCalc;
import org.bb.constants.Stickers;
import org.bb.constants.TypeMessage;
import java.util.ArrayList;
import static org.bb.constants.text.IsodoseCalcText.ERROR_DOSE;
import static org.bb.constants.text.IsodoseCalcText.EXIT;

public class IsodoseCalcActivity implements IActivity {
    @Override
    public ArrayList<Message> run(Message m) {
        ArrayList<Message> messages = new ArrayList<>();
        String output = IsodoseCalc.calculate(m.text);
        if (output.isEmpty()){
            messages.add(new Message(m.user, Stickers.randomSticker(), TypeMessage.STICKER));
            messages.add(new Message(m.user, ERROR_DOSE.value.concat(EXIT.value)));
        }
        else {
            messages.add(new Message(m.user, output.concat(EXIT.value)));
        }
        return messages;
    }
}
