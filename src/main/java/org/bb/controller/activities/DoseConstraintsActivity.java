package org.bb.controller.activities;

import org.bb.constants.doseconstraints.Sites;
import org.bb.constants.doseconstraints.Fractions;
import org.bb.db.DB;
import org.bb.constants.doseconstraints.OrgansList;
import org.bb.constants.Stickers;
import org.bb.controller.Functions;
import org.bb.controller.Message;
import java.util.ArrayList;
import static org.bb.constants.TypeMessage.MENU;
import static org.bb.constants.TypeMessage.STICKER;
import static org.bb.constants.text.DoseContstraintsText.*;

public class DoseConstraintsActivity implements IActivity {
    private String current = "fractions";
    private Fractions userFraction;
    private Sites userSite;
    private String userOrgan;

    private DoseConstraintsActivity(){}
    public DoseConstraintsActivity(ArrayList<Message> messages, Message m){
        messages.add(new Message(m.user, Fractions.getAllValues(), MENU, "Number of fractions:"));
    }
    @Override
    public ArrayList<Message> run(Message m) {
        ArrayList<Message> messages = new ArrayList<>();
        if (current.equals("fractions")) {
            for (Fractions f : Fractions.values()){
                if (f.value.equals(m.text)){
                    userFraction = f;
                    current = "sites";
                    messages.add(new Message(m.user, Sites.getAllValues(), MENU, SELECT_SITE.value));
                    break;
                }
            }
        }
        if (current.equals("sites")) {
            for (Sites st : Sites.values()){
                if (st.value.equals(m.text)){
                    userSite = st;
                    current = "case";
                    String result = ALL.value + "," + DB.filterFractionSite(userFraction, userSite);
                    messages.add(new Message(
                            m.user, result, MENU, AVAILABLE.value));
                    break;
                }
            }
        }
        if (current.equals("case")){
            if (m.text.equals(ALL.value)){
                ArrayList<String> all = Functions.stringToList(DB.filterFractionSite(userFraction, userSite));
                for (String s : all){
                    for (String or : OrgansList.get()){
                        if (or.equals(s)){
                            userOrgan = or;
                            String result = getConstraintsTitle() + DB.getOrgan(userOrgan, userFraction);
                            messages.add(new Message(m.user, result));
                            break;
                        }
                    }
                }
                m.user.setActivity(new DoseConstraintsActivity());
                messages.add(new Message(m.user, Fractions.getAllValues(), MENU, CONTINUE_OR_EXIT.value));
            }
            for (String or : OrgansList.get()){
                if (or.equals(m.text)){
                    userOrgan = or;
                    String result = getConstraintsTitle() + DB.getOrgan(userOrgan, userFraction);
                    m.user.setActivity(new DoseConstraintsActivity());
                    messages.add(new Message(m.user, result));
                    messages.add(new Message(m.user, Fractions.getAllValues(), MENU, CONTINUE_OR_EXIT.value));
                    break;
                }
            }
        }
        if (messages.isEmpty()){
            messages.add(new Message(m.user, Stickers.randomSticker(), STICKER));
            messages.add(new Message(m.user, NOT_FOUND.value));
        }
        return messages;
    }
    private String getConstraintsTitle(){
        return "<b>" + userOrgan.toUpperCase() + " for " + userFraction.value + " fractions:</b>\n\n";
    }
}
