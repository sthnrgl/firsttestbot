package org.bb.controller.activities;
import org.bb.Login;
import org.bb.constants.StickersNonPublic;
import org.bb.constants.text.BEDtext;
import org.bb.constants.text.IsodoseCalcText;
import org.bb.controller.Message;
import org.bb.constants.text.MainMenu;
import org.bb.constants.text.TreatmentDelayText;
import org.bb.user.ActiveUsersMap;
import java.util.ArrayList;
import static org.bb.constants.TypeMessage.MENU;
import static org.bb.constants.TypeMessage.STICKER;
import static org.bb.constants.text.MainMenu.*;

public class MainMenuActivity implements IActivity {
    private final static ActiveUsersMap ACTIVE_USERS_MAP = ActiveUsersMap.instance;
    @Override
    public ArrayList<Message> run(Message m) {
        ArrayList<Message> messages = new ArrayList<>();

        if (m.text.equals(DOSE_CONSTRAINTS.value)){
            m.user.setActivity(new DoseConstraintsActivity(messages, m));
            return messages;
        }
        if (m.text.equals(BED.value)){
            m.user.setActivity(new BEDActivity());
            messages.add(new Message(m.user, BEDtext.ENTER.value));
            return messages;
        }
        if (m.text.equals(ISODOSE_CALC.value)){
            m.user.setActivity(new IsodoseCalcActivity());
            messages.add(new Message(m.user, IsodoseCalcText.ENTER.value));
            return messages;
        }
        if (m.text.equals(SEND.value)){
            m.user.setActivity(new SendMessageActivity());
            messages.add(new Message(m.user, "id text"));
            return messages;
        }
        if (m.text.equals(TREATMENT_DELAYS.value)){
            m.user.setActivity(new TreatmentDelayActivity());
            messages.add(new Message(m.user, TreatmentDelayText.questionPlannedFractions));
            return messages;
        }


        else {
            if (m.text.equals(LIST_OF_USERS.value) && m.user.getPermissions() == 666){
                messages.add(new Message(m.user, ActiveUsersMap.getListOfActiveUsers()));
            }
            if (m.text.equals(Login.getAdminPass())){
                messages.add(new Message(m.user, StickersNonPublic.YA_YIBAV.value, STICKER));
                m.user.setPermissions(666);
            }
            if (m.text.equals(LOGOUT.value)){
                messages.add(new Message(m.user, StickersNonPublic.VAN.value, STICKER));
                messages.add(new Message(m.user, "Slave! Get your ass back here!"));
                m.user.setPermissions(0);
            }
            if (m.user.getPermissions() == 666)
                messages.add(new Message(m.user, MAIN_MENU_ADMIN_LIST.value, MENU, MAINMENU.value));
            else
                messages.add(new Message(m.user, MainMenu.MAIN_MENU_LIST.value, MENU, MAINMENU.value));
            return messages;
        }

    }
}
