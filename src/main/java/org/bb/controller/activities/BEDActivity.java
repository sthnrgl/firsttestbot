package org.bb.controller.activities;

import org.bb.controller.Message;
import org.bb.apps.BED;
import org.bb.constants.Stickers;
import org.bb.constants.TypeMessage;
import java.util.ArrayList;
import static org.bb.constants.text.BEDtext.INCORRECT_NUMBER_ARGS;

public class BEDActivity implements IActivity {

    @Override
    public ArrayList<Message> run(Message m) {
        ArrayList<Message> messages = new ArrayList<>();
        String output = BED.calculate(m.text);
        if (output.isEmpty() || output.equals(INCORRECT_NUMBER_ARGS.value)){
            messages.add(new Message(m.user, Stickers.randomSticker(), TypeMessage.STICKER));
            messages.add(new Message(m.user, output));
        }
        else
            messages.add(new Message(m.user, output));
        return messages;
    }

}
