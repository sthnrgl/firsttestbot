package org.bb.controller.activities;

import org.bb.apps.TreatmentDelay;
import org.bb.constants.Stickers;
import org.bb.constants.TypeMessage;
import org.bb.constants.text.MainMenu;
import org.bb.constants.text.TreatmentDelayText;
import org.bb.controller.Functions;
import org.bb.controller.Message;

import java.util.ArrayList;


public class TreatmentDelayActivity implements IActivity{
    private double fractionPlanned,
            doseFr,
            fractionDelivered,
            delay,
            ab,
            proliferation;
    private String current = "fractionPlanned";
    ArrayList<Double> digits;

    @Override
    public ArrayList<Message> run(Message m) {
        ArrayList<Message> messages = new ArrayList<>();
        digits = Functions.parseDigits(m.text);
        if (current.equals("fractionPlanned")) {
            if (digits.isEmpty()){
                messages.add(new Message(m.user, Stickers.randomSticker(), TypeMessage.STICKER));
                messages.add(new Message(m.user, TreatmentDelayText.errorNoNumber));
            } else {
                fractionPlanned = digits.getFirst();
                current = "dosePerFraction";
                messages.add(new Message(m.user, TreatmentDelayText.questionDosePerFraction));
            }
            return messages;
        }

        if (current.equals("dosePerFraction")){
            if (digits.isEmpty()){
                messages.add(new Message(m.user, Stickers.randomSticker(), TypeMessage.STICKER));
                messages.add(new Message(m.user, TreatmentDelayText.errorNoNumber));
            } else {
                doseFr = digits.getFirst();
                current = "fractionDelivered";
                messages.add(new Message(m.user, TreatmentDelayText.questinFractionsDelivered));
            }
            return messages;
        }

        if (current.equals("fractionDelivered")){
            if (digits.isEmpty()){
                messages.add(new Message(m.user, Stickers.randomSticker(), TypeMessage.STICKER));
                messages.add(new Message(m.user, TreatmentDelayText.errorNoNumber));
            } else {
                fractionDelivered = digits.getFirst();
                current = "delay";
                messages.add(new Message(m.user, TreatmentDelayText.questionDelay));
            }
            return messages;
        }

        if (current.equals("delay")){
            if (digits.isEmpty()){
                messages.add(new Message(m.user, Stickers.randomSticker(), TypeMessage.STICKER));
                messages.add(new Message(m.user, TreatmentDelayText.errorNoNumber));
            } else {
                delay = digits.getFirst();
                current = "proliferation";
                messages.add(new Message(m.user, TreatmentDelayText.proliferationMenu, TypeMessage.MENU, TreatmentDelayText.proliferationSelect));
            }
            return messages;
        }

        if (current.equals("proliferation")){
            digits = Functions.parseDigits(m.text);
            if (digits.isEmpty()){
                messages.add(new Message(m.user, Stickers.randomSticker(), TypeMessage.STICKER));
                messages.add(new Message(m.user, TreatmentDelayText.errorNoNumber));
            } else {
                proliferation = digits.getFirst();
                current = "ab";
                messages.add(new Message(m.user, TreatmentDelayText.questionAB));
            }
            return messages;
        }
        if (current.equals("ab")){
            if (digits.isEmpty()){
                messages.add(new Message(m.user, Stickers.randomSticker(), TypeMessage.STICKER));
                messages.add(new Message(m.user, TreatmentDelayText.errorNoNumber));
            } else {
                ab = digits.getFirst();
                current = "fractionPlanned";
                String s = TreatmentDelay.calculate(fractionPlanned,doseFr, fractionDelivered, delay, ab, proliferation);
                messages.add(new Message(m.user, s));
                messages.add(new Message(m.user, MainMenu.MAINMENU.value, TypeMessage.MENU, TreatmentDelayText.questionPlannedFractions));
            }

            return messages;
        }

        return messages;
    }
}
