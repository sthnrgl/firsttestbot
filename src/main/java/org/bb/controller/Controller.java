package org.bb.controller;

import org.bb.Login;
import org.bb.constants.StickersNonPublic;
import org.bb.constants.TypeMessage;
import org.bb.user.ActiveUsersMap;
import org.bb.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.api.objects.Update;
import java.util.ArrayList;

import static org.bb.settings.Settings.*;

public class Controller {
    private final static ActiveUsersMap ACTIVE_USERS_MAP = ActiveUsersMap.instance;
    private final static Logger log = LoggerFactory.getLogger(Controller.class);
    private final Boolean showUpdatesInLog = getShowUpdatesInLog(),
                    forwardUpdatesToAdmin = getForwardUpdatesToAdmin(),
                    showAnswerInLog = getShowAnswerInLog(),
                    forwardAnswerToAdmin = getForwardAnswerToAdmin();


    public ArrayList<Message> run(Update update){
        String id = update.getMessage().getFrom().getId().toString();
        ArrayList<Message> messages = new ArrayList<>();

        if (id.equals(Login.getSuperUserID())) {
            registerSuperUser(id, update);
        }
        if (!ACTIVE_USERS_MAP.isUserExist(id)){
            ACTIVE_USERS_MAP.addUser(id, update);
            log.info("""
                            
                            |-------------------
                            | Create new user:
                            |___________________
                            {}
                            """,
                    ACTIVE_USERS_MAP.getUser(id).toString());
        }
        User user = ACTIVE_USERS_MAP.getUser(id);

        if (update.getMessage().hasText()){
            Message m = new Message(user, update.getMessage().getText());
            if (new GlobalCommands().checkGlobalCommands(m, messages)){
                messages.addAll(user.getActivity().run(m));
            }
                //TODO check non-commands

            logging(m, messages);
        }
        else {
            messages.add(new Message(user, StickersNonPublic.PODEREV_JOKE_FAIL.value, TypeMessage.STICKER));
        }
        return messages;
    }
    private void registerSuperUser(String id, Update update){
        if (!ACTIVE_USERS_MAP.isUserExist(id)){
            ACTIVE_USERS_MAP.addUser(id, update);
            ACTIVE_USERS_MAP.getUser(id).setPermissions(666);
            log.info("\n\tПідйом, бляді, доктор прийшов!");
        }
    }
    private void logging(Message m, ArrayList<Message> messages){
        User admin = null;
        if (ACTIVE_USERS_MAP.isUserExist(Login.getSuperUserID())){
            admin = ACTIVE_USERS_MAP.getUser(Login.getSuperUserID());
        }
        if (showUpdatesInLog)
            log.info("\n{} {} ->\t{}", m.user.ID, m.user.firstName, m.text);
        if (showAnswerInLog){
            for (Message ans : messages)
                log.info("\t{} {} <-\t{}", ans.user.ID, ans.user.firstName, ans.text);
        }
        if (forwardUpdatesToAdmin){
            String s =  String.format("From: %s - %s ( %s ):",
                    m.user.ID, m.user.firstName, m.user.nickName);
            messages.add( new Message(admin, s));
        }
        if (forwardAnswerToAdmin){
            ArrayList<Message> forward = new ArrayList<>();
            for (Message msg : messages){
                String s = String.format("To: %s - %s ( %s ):",
                        m.user.ID, m.user.firstName, m.user.nickName);
                forward.add(new Message(admin, s));
                forward.add(msg.forwardTo(admin));
            }
            messages.addAll(forward);
        }
    }
}
