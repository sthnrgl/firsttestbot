package org.bb.controller;

import org.bb.settings.Settings;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Functions {

    public static ArrayList<String> stringToList(String items){
        return new ArrayList<>(Arrays.asList(items.split(Settings.MENU_SPLITTER)));
    }
    public static ArrayList<String> splitText(String message){
        ArrayList<String> list = new ArrayList<>();
        while (true){
            if (message.length() <= 4000){
                list.add(message);
                break;
            }
            else {
                list.add(message.substring(0, 3999));
                message = message.substring(3999);
            }
        }
        return list;
    }
    public static ArrayList<String> fileToList(String filePath){
        ArrayList<String> list = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line = reader.readLine();
            while (line != null){
                list.add(line);
                line = reader.readLine();
            }
            reader.close();
        } catch (RuntimeException | IOException e) {
            System.out.println(e.getMessage());
        }

        return list;
    }
    public static ArrayList<Double> parseDigits(String input){
        final Pattern p = Pattern.compile("\\d+[.]?\\d*");
        ArrayList<Double> result = new ArrayList<>();
        if (input == null)
            return result;
        Matcher m = p.matcher(input);
        while (m.find()){
            result.add(Double.parseDouble(m.group()));
        }
        return result;
    }
    public static String safeHTML(String s){
        /**
         * Convert the characters &, < and > in string s to HTML-safe sequences.
         *  "<" = "&lt;"
         *  ">" = "&gt;"
         *  "&" = "&amp;"
         *  */
        s = s.replace("&", "&amp;").
                replace("<", "&lt;").
                replace(">", "&gt;");
        return s;
    }
}
