package org.bb.controller.logbot;

import org.bb.Login;
import org.bb.controller.Message;
import org.bb.db.DB;
import org.bb.user.ActiveUsersMap;
import org.bb.user.User;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.ArrayList;

public class ControllerLogBot {
    private final static ActiveUsersMap USERS = ActiveUsersMap.instance;
    private final ArrayList<Message> messages = new ArrayList<>();
    private User superUser;

    private User registerUser(Update update){
        String id = update.getMessage().getFrom().getId().toString();
        if (!USERS.isUserExist(id)) {
            USERS.addUser(id, update);

            if (id.equals(Login.getSuperUserID())){
                superUser = USERS.getUser(id);
                superUser.setPermissions(666);
                messages.add(new Message(superUser, "superuser registered"));
            }
        }
        System.out.println(USERS.getUser(id).toString());
        return USERS.getUser(id);
    }
    public ArrayList<Message> run(Update update){
        if (!update.hasMessage() || !update.getMessage().hasText())
            return messages;
        User user = registerUser(update);
        if (!user.ID.equals(Login.getSuperUserID())){
            messages.add(
                    new Message(user, "ти ще хто"));
            /*messages.add(
                    new Message(superUser, STR."якийсь мамкин хакер ломиться,\n\{user.toString()}")
            );*/
        }
        messages.add(
                new Message(user, user.toString())
        );
        return messages;
    }
}
