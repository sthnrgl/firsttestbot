package org.bb.controller;

import org.bb.apps.Alphabeta;
import org.bb.apps.DickCounter;
import org.bb.apps.Sources;
import org.bb.constants.StickersNonPublic;
import org.bb.constants.TypeMessage;
import org.bb.controller.activities.MainMenuActivity;

import java.util.ArrayList;

import static org.bb.constants.Stickers.*;
import static org.bb.constants.TypeMessage.STICKER;
import static org.bb.constants.text.MainMenu.*;
/**
 * These commands are available from anywhere in the bot interface
 * */
public class GlobalCommands {
    public boolean checkGlobalCommands(Message m, ArrayList<Message> messages){
        String t = m.text;
        if (DickCounter.checkDick(m, messages))
            return false;

        if (t.equals(MAINMENU.value)){
            /**
             * Return to the main menu state
             * */
            m.user.setActivity(new MainMenuActivity());
            return true;
        }
        if (t.equals(HELP.value)){
            String ss = "res/video/help.mp4";
            messages.add(new Message(m.user, ss, TypeMessage.VIDEO));
            messages.add(new Message(m.user, "Return to /menu or continue entering data:"));
            return false;
        }
        if (t.equals(ALPHABETA.value) || t.equals(ALPHABETA_2.value)){
            messages.add(new Message(m.user, Alphabeta.alphaBetaTable));
            messages.add(new Message(m.user, "Return to /menu or continue entering data:"));
            return false;
        }
        if (t.equals(SOURCE.value)){
            messages.add(new Message(m.user, Sources.sourcesList));
            messages.add(new Message(m.user, "Return to /menu or continue entering data:"));
            return false;
        }
        if (t.equals("бля") || (t.equals("Бля") || t.equals("блядь") || t.equals("блять"))){
            messages.add(new Message(m.user, StickersNonPublic.PODEREV_FILIN.value, STICKER));
            return false;
        }
        return true;
    }
}
