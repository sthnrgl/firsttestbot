package org.bb.controller;

import org.bb.constants.TypeMessage;
import org.bb.settings.Settings;
import org.bb.user.User;

public class Message {
    public final User user;
    public final String text,
                        parseMode = Settings.getParseMode(),
                        caption;
    public final TypeMessage typeMessage;


    public Message(User user, String text) {
        this.user = user;
        this.text = text;
        this.typeMessage  = TypeMessage.TEXT;
        this.caption = null;
    }

    public Message(User user, String text, TypeMessage typeMessage) {
        this.user = user;
        this.text = text;
        this.typeMessage = typeMessage;
        this.caption = null;
    }
    public Message(User user, String text, TypeMessage typeMessage, String caption) {
        this.user = user;
        this.text = text;
        this.typeMessage = typeMessage;
        this.caption = caption;
    }

    public Message forwardTo(User user){
        return new Message(user, text, typeMessage ,caption);
    }

    @Override
    public String toString() {
        return String.format("""
                User:   %s - %s
                Text:   %s
                Type:   %s
                Parser: %s
                """, user.ID, user.firstName, text, typeMessage, parseMode);
    }
}
