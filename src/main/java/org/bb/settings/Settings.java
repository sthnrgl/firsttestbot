package org.bb.settings;

import org.bb.db.DB;

import java.util.Objects;

public class Settings {
    public static final String MENU_SPLITTER = ",",
                               DEFAULT_CAPTION = "-----";
    public static String getParseMode() {
        return DB.getSettings("ParseMode");
    }
    public static Boolean getShowUpdatesInLog() {
        return Boolean.valueOf(DB.getSettings("ShowUpdatesInLog"));
    }
    public static Boolean getForwardUpdatesToAdmin() {
        return Boolean.valueOf(DB.getSettings("ForwardUpdatesToAdmin"));
    }
    public static Boolean getShowAnswerInLog() {
        return Boolean.valueOf(DB.getSettings("ShowAnswerInLog"));
    }
    public static Boolean getForwardAnswerToAdmin(){
        return Boolean.valueOf(DB.getSettings("ForwardAnswerToAdmin"));
    }
    public static long sleepScheduledTasks(){
        return Long.parseLong(Objects.requireNonNull(DB.getSettings("SleepScheduledTasks")));
    }

    public static String showSettings(){
        return String.format("""
                Settings:
                parse mode: %s
                ShowUpdatesInLog: %s
                ShowAnswerInLog: %s
                ForwardUpdatesToAdmin: %s
                ForwardAnswerToAdmin: %s
                sleepScheduledTasks: %s
                """,
                getParseMode(),
                getShowUpdatesInLog(),
                getShowAnswerInLog(),
                getForwardUpdatesToAdmin(),
                getForwardAnswerToAdmin(),
                sleepScheduledTasks()
                );
    }
}
