package org.bb;

import org.bb.controller.Functions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Login {
    /**
     *  Console args:
     *  1) bot name
     *  2) bot token
     *  3) superuser telegram id
     *  4) db url;
     *  5) login to db
     *  6) pass to db
     *  7) pass for login as admin (in main menu)
     * */
    private static final Logger log = LoggerFactory.getLogger(Login.class);
    private static String botName1, botKey1, botName2, botKey2, superUserID, dbUrl, dbLogin, dbPass, adminPass;
    public static void getLoginsFromConsole(String[] args){
        if (args == null){
            log.error("noLoginArguments");
            throw new RuntimeException();
        }
        Pattern pattern = Pattern.compile("[^ ]+");
        List<String> result = new ArrayList<>();
        Matcher matcher;
        for (String s : args){
            matcher = pattern.matcher(s);
            while (matcher.find())
                result.add(matcher.group());
        }
        if (result.size() == 9){
            botName1 =      result.get(0);
            botKey1 =       result.get(1);
            botName2 =      result.get(2);
            botKey2 =       result.get(3);
            superUserID =   result.get(4);
            dbUrl =         result.get(5);
            dbLogin =       result.get(6);
            dbPass =        result.get(7);
            adminPass =     result.get(8);
        }
        else
            log.error("wrong hole");
    }
    public static void getLoginsFromFile(String fileName){
        ArrayList<String> logins = Functions.fileToList(fileName);
        if (logins.size() == 9){
            botName1 =      logins.get(0);
            botKey1 =       logins.get(1);
            botName2 =      logins.get(2);
            botKey2 =       logins.get(3);
            superUserID =   logins.get(4);
            dbUrl =         logins.get(5);
            dbLogin =       logins.get(6);
            dbPass =        logins.get(7);
            adminPass =     logins.get(8);
        }
    }
    public static String getBotName1() {
        return botName1;
    }
    public static String getBotKey1() {
        return botKey1;
    }
    public static String getBotName2(){
        return botName2;
    }
    public static String getBotKey2(){
        return botKey2;
    }
    public static String getSuperUserID(){return superUserID;}
    public static String getDbUrl(){
        return dbUrl;
    }
    public static String getDbLogin() {
        return dbLogin;
    }
    public static String getDbPass() {
        return dbPass;
    }
    public static String getAdminPass(){
        return adminPass;
    }
}