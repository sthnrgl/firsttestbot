package org.bb.constants;

public enum TypeMessage {
    TEXT,
    MENU,
    PICTURE,
    VIDEO,
    STICKER
}
