package org.bb.constants;
import java.util.ArrayList;
import java.util.Random;

import static org.bb.controller.Functions.fileToList;

public class Stickers {
    private static final Random RANDOM = new Random();
    private static final ArrayList<String> list = fileToList("res/stickers/stickers.txt");

    public static String randomSticker()  {
        return list.get(RANDOM.nextInt(list.size()));
    }
}
