package org.bb.constants.text;

public enum MainMenu {
    //"menu,dose constraints,BED,isodose calc,α/β,help,sources,back"
    MAINMENU("/menu"),
    DOSE_CONSTRAINTS("឵dose constraints"),
    BED("឵BED"),
    TREATMENT_DELAYS("⚠\uFE0F treatment delays"),
    ISODOSE_CALC("឵isodose calc"),
    ALPHABETA("α/β"),
    ALPHABETA_2("/alphabeta"),
    HELP("/help"),
    SOURCE("/source"),
    LIST_OF_USERS("active users"),
    LOGOUT("logout"),
    SEND("send to"),
    MAIN_MENU_LIST(String.format("%s, %s, %s, %s, %s, %s",
            DOSE_CONSTRAINTS.value,
            BED.value,
            TREATMENT_DELAYS.value,
            ALPHABETA.value,
            HELP.value,
            SOURCE.value
    )),
    MAIN_MENU_ADMIN_LIST(String.format("%s, %s, %s, %s, %s, %s, %s, %s, %s, %s",
            DOSE_CONSTRAINTS.value,
            BED.value,
            TREATMENT_DELAYS.value,
            ISODOSE_CALC.value,
            ALPHABETA.value,
            LIST_OF_USERS.value,
            SEND.value,
            LOGOUT.value,
            HELP.value,
            SOURCE.value
    ));
    public final String value;
    MainMenu(String value){
        this.value = value;
    }
    public String getValue(){
        return value;
    }

}
