package org.bb.constants.text;

public class TreatmentDelayText {
    public static final String
            proliferationDefault = "Значення не знайдено, буде використано k = 0.3",
            proliferationMenu = "low (0.1),medium (0.3),high (0.5)",
            proliferationSelect = "Коефіцієнт проліферації. Можна замість вибору кнопки ввести своє значення",
            errorNoNumber = "Потрібно ввести число. Через крапку, коми не підходять",
            questionPlannedFractions = "Кількість запланованих фракцій:",
            questionDosePerFraction = "Запланована доза за фракцію (Gy):",
            questinFractionsDelivered = "Скільки фракцій проліковано?",
            questionDelay = "Скільки днів перерви лікування?",
            questionAB = "α/β? (показати таблицю /alphabeta)",

            result = """
                ❗❗❗ Перевірте правильність введених даних ❗❗❗
                fraction planned   %.0f
                dose per fraction  %.2f Gy/f
                fractionDelivered  %.0f
                delay                       %.0f days
                α/β                          %.2f
                proliferation rate  %.1f
                ☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐
                Dose planned         %.1f Gy
                BED planned       ≈ %.1f Gy
                Dose delivered       %.1f Gy
                BED delivered     ≈ %.1f Gy
                
                BED to add          ≈ %.1f Gy
                
                ☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐
                <b>RESULT</b>
                
                Якщо доза за фракцію не змінюється (%.2f):
                ✅ нова кількість фракцій ≈ %.0f
                
                Якщо кількість фракцій що залишилось не змінюється (%.0f):
                ✅ нова доза за фракцію ≈ %.2f Gy/f
                """;





}
