package org.bb.constants.text;

public enum BEDtext {
    ENTER("""
            <b>BED calculator</b>
            
            Enter 3 numbers:
            FRACTIONS, DOSE/FRACTION and α/β
            ex.: 25 2 10
                       
            /alphabeta - show α/β table
            
            /menu - exit
            """),
    CORRECT("<b>Did you mean %.2f per fraction?</b>\n\n"),
    INVALID_FRAC("""
            Invalid # of fractions: LQ model operates in range from %.0f to %.0f fractions
            Try again or /menu to exit
            """),
    WARNING_FRAC("""
            ❗❗❗ Warning # of fractions: LQ model operates in range from %.0f to %.0f fractions
            
            """),
    INVALID_DOSE_PER_FRAC("""
            Invalid fraction dose: LQ model operates in dose range from %.2f to %.2f Gy per fraction
            Try again or /menu to exit
            """),
    WARNING_DOSE_PER_FRAC("""
            ❗❗❗ Warning fraction dose: LQ model operates in dose range from %.2f to %.2f Gy per fraction
            
            """),
    INVALID_AB("""
            Invalid alpha/beta ratio: reliable α/β values are in range from %.2f to %.2f
            "Try again or /menu to exit
            """),
    OUT_FORMAT("""
            Fractionation scheme:
            fraction = %.0f
            dose per fraction = %.2f
            α/β = %.1f
            dose of irradiation = %.2f
            <b>BED</b> = %.2f Gy
            <b>EQD2</b> = %.2f Gy  ➔  approx. 2Gy*%d
            Try again or /menu to exit
            """),
    INCORRECT_NUMBER_ARGS("""
            Incorrect number of arguments (must be 3)
            Try again or /menu to exit
            """);
    public final String value;
    BEDtext(String value) {

        this.value = value;
    }
}
