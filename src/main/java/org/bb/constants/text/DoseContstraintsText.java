package org.bb.constants.text;

public enum DoseContstraintsText {
    SELECT_SITE("Select site"),
    ALL("ALL"),
    AVAILABLE("Available constraints"),
    CONTINUE_OR_EXIT("Continue or /menu for exit"),
    NOT_FOUND("Not found\nTry again");

    public final String value;
    DoseContstraintsText(String value){
        this.value = value;
    }
}
