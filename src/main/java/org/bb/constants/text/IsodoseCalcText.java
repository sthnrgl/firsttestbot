package org.bb.constants.text;

public enum IsodoseCalcText {
    ENTER("""
            Isodose calculation
            Enter doses (Gy):
            (ex. 45 50 66)
            """),
    FORMAT_ISODOSE_CALC("""
            107%%  ->  %.3f Gy
            105%%  ->  %.3f Gy
            <b>100%%  ->  %.3f Gy</b>
            97%%  ->  %.3f Gy
            95%%    ->  %.3f Gy
            80%%    ->  %.3f Gy
            ----------------------------------------
            """),
    NOT_ENOUGH("\n<i>ACHTUNG! Geringe Strahlendosis!</i>\n\n"),
    TO_MUCH("\n<b>ACHTUNG! Hohe Strahlendosis!</b>\n\n"),
    EXIT("\nEnter another dose or /menu to exit\n"),
    ERROR_DOSE("Valid dose - digits, ex: 47.5 50 70");
    public final String value;

    IsodoseCalcText(String value) {
        this.value = value;
    }
}
