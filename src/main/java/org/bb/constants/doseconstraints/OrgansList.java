package org.bb.constants.doseconstraints;

import org.bb.db.DB;
import org.bb.settings.Settings;

import java.util.ArrayList;

public class OrgansList {
    private static ArrayList<String> list = DB.getAllOrgans();

    public static ArrayList<String> get(){
        return list;
    }
}
