package org.bb.constants.doseconstraints;

import org.bb.settings.Settings;

public enum Fractions {
    f1("1"),
    f2("2"),
    f3("3"),
    f4("4"),
    f5_7("5-7"),
    f8_9("8-9"),
    f10_14("10-14"),
    f15("15"),
    f20("20"),
    f16_40("16-40");

    public final String value;
    public static String getAllValues(){
        Fractions[] values = Fractions.values();
        StringBuilder sb = new StringBuilder();
        for (Fractions fraction : values)
            sb.append(fraction.value).append(Settings.MENU_SPLITTER);
        return sb.substring(0, sb.toString().length() - 1);
    }
    Fractions(String value){
        this.value = value;
    }
}
