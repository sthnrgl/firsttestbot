package org.bb.constants.doseconstraints;

import org.bb.settings.Settings;

public enum Sites {
    HEAD("head"),
    HEAD_AND_NECK("head and neck"),
    THORAX("thorax"),
    ABDOMEN("abdomen"),
    PELVIS("pelvis");
    public final String value;
    public static String getAllValues(){
        Sites[] values = Sites.values();
        StringBuilder sb = new StringBuilder();
        for (Sites site : values)
            sb.append(site.value).append(Settings.MENU_SPLITTER);
        return sb.substring(0, sb.toString().length() - 1);
    }
    Sites(String value) {
        this.value = value;
    }
}
