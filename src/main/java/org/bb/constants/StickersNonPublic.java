package org.bb.constants;

public enum StickersNonPublic {
    YA_YIBAV("CAACAgIAAxkBAAEJ5yhky2CCucA7gn016PzWjHW0wSAHYwACXyEAAsdp-UmQqkq9UvM-9S8E"),
    PODEREV_FILIN("CAACAgIAAxkBAAEJ50Bky2LaWnakBofxaboNsekFgB-rswACIAIAAvEElxNH-xhoJ8VZcy8E"),
    PODEREV_JOKE_FAIL("CAACAgIAAxkBAAEJ50hky2UfphbBzYx9ksC9jseJ4gj_zQACRAIAAvEElxOrsAEqhjj4fy8E"),
    VAN("CAACAgIAAxkBAAEKMexk8le-Ab1IedOPPN3yI0vspskAAUgAAvMMAAK0Q1FK7gqNBSETevAwBA");

    public final String value;
    StickersNonPublic(String value){
        this.value = value;
    }
}
