package org.bb.apps;

import org.bb.controller.Functions;

import java.util.List;

import static org.bb.constants.text.IsodoseCalcText.*;

public class IsodoseCalc {
    public static String calculate(String doses){
        List<Double> input = Functions.parseDigits(doses);
        StringBuilder sb = new StringBuilder();
        for (double d : input){
            if (d < 10)
                sb.append(NOT_ENOUGH.value);
            if (d > 70)
                sb.append(TO_MUCH.value);

            sb.append(String.format(
                    FORMAT_ISODOSE_CALC.value,(d * 1.07), (d * 1.05), d, (d * 0.97), (d * 0.95), (d * 0.8)
            ));
        }
        return sb.toString();
    }
}
