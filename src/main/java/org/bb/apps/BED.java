package org.bb.apps;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static org.bb.controller.Functions.parseDigits;
import static org.bb.constants.text.BEDtext.*;

public class BED {
    public static final double minFrac = 1,
                        minfracWarning = 8, // 1
                        maxFrac = 40,
                        minDose = 0.1,
                        maxDose = 30, // 30
                        maxDoseWarning = 8,
                        minAB = 0.5,  // 0.1
                        maxAB = 100;

    public static String calculate(String input) {
        List<Double> list = parseDigits(input);
        if (list.size() == 3){
            double fractions = list.get(0),
                    dosePerFraction = list.get(1),
                    alphabeta = list.get(2);
            String correction = "";
            if (dosePerFraction > 25) {
                dosePerFraction = dosePerFraction / fractions;
                correction = String.format(CORRECT.value, dosePerFraction);
            }
            if (fractions < minFrac || fractions > maxFrac)
                return String.format(INVALID_FRAC.value, minfracWarning, maxFrac);
            if (fractions < minfracWarning)
                correction = correction + String.format(WARNING_FRAC.value, minfracWarning, maxFrac);

            if (dosePerFraction < minDose || dosePerFraction > maxDose)
                return String.format(INVALID_DOSE_PER_FRAC.value, minDose, maxDose);
            if (dosePerFraction > maxDoseWarning)
                correction = correction + String.format(WARNING_DOSE_PER_FRAC.value, minDose, maxDoseWarning);

            if (alphabeta < minAB || alphabeta > maxAB)
                return String.format(INVALID_AB.value, minAB, maxAB);

            else{
                double deliveredDose = fractions * dosePerFraction;
                double BED = fractions * dosePerFraction * (1 + dosePerFraction / alphabeta);
                double EQD2 = fractions * dosePerFraction * (alphabeta + dosePerFraction)/(2 * alphabeta + 4);
                return  correction.concat(
                        String.format(OUT_FORMAT.value,
                                fractions,
                                dosePerFraction,
                                alphabeta,
                                deliveredDose,
                                BED,
                                EQD2*2,
                                BigDecimal.valueOf(EQD2).setScale(0, RoundingMode.HALF_UP).intValue()
                        ));
            }
        }
        return INCORRECT_NUMBER_ARGS.value;
    }
}
