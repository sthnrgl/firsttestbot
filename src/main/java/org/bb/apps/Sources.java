package org.bb.apps;

import java.util.stream.Collectors;

import static org.bb.controller.Functions.fileToList;

public class Sources {
    public final static String sourcesList =
            fileToList("res/data/sources.txt").stream().collect(Collectors.joining("\n\n"));
}
