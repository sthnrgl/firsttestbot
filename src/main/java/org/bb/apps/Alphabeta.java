package org.bb.apps;

import java.util.stream.Collectors;

import static org.bb.controller.Functions.fileToList;

public class Alphabeta {
    public final static String alphaBetaTable =
            fileToList("res/data/alphabeta.txt").stream().collect(Collectors.joining("\n\n"));
}
