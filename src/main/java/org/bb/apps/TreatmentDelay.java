package org.bb.apps;

import org.bb.constants.text.TreatmentDelayText;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class TreatmentDelay {
    /**
     * BED = N * d * (1 + d / (a/b)) - k * T_delay
     * N - fractions
     * d - dose per fraction
     * T_delay - treatment delay? days
     * k : 0.1 0.3 0.5 (or old value 0.3 0.9 1.2)
     * */
    public static String calculate(
            double fractionPlanned,
            double doseFr,
            double fractionDelivered,
            double delay,
            double ab,
            double proliferation){
        double
                BEDplanned,
                BEDdelivered,
                BEDrest,
                k = proliferation,
                fractionX,
                doseX1, doseX2;

        if (fractionPlanned < 1 || fractionPlanned > 40)
            return "кількість запланованих фракцій має бути між 1 і 40";
        if (doseFr == 0)
            return "0 грей за фракцію?";
        if (doseFr > 40)
            return "більше 40 грей за фракцію";
        if (fractionDelivered == 0)
            return "0 доставлених фракцій - лікування ще не почалось";
        if (fractionDelivered == fractionPlanned)
            return "кількість доставлених фракцій рівна запланованим, лікування закінчено";
        if (fractionDelivered > fractionPlanned)
            return "вже доставлено більше фракцій ніж заплановано, ой";
        if (delay > fractionPlanned)
            return "модель не працює, якщо кількість пропущених фракцій більша ніж кількість запланованих.";
        if (delay == 0)
            return "0 днів пропуску, немає що перераховувати";
        if (ab == 0)
            return "α/β не може бути нульовим";
        if (ab > 100)
            return "α/β має бути між 0.1 i 100";
        if (k == 0 || k > 3)
            return "proliferation rate = 0 або занадто велике значення";

        if (fractionPlanned < 0 || doseFr < 0 || fractionDelivered < 0 || delay < 0 || ab < 0)
            return "напишіть розробнику як вам вдалось ввести від'ємне значення";


        BEDplanned = fractionPlanned * doseFr * (1 + doseFr / ab);
        BEDdelivered = fractionDelivered * doseFr * (1 + doseFr / ab);
        BEDrest = BEDplanned - BEDdelivered + k * delay;

        fractionX = BEDrest / (doseFr * (1 + doseFr / ab));
        double fractionRest = fractionPlanned - fractionDelivered,
                D = ab * ab + 4 * BEDrest * ab / fractionRest;
        doseX1 = (-ab + Math.sqrt(D)) / 2;
        doseX2 = (-ab - Math.sqrt(D)) / 2;
        if (doseX1 < doseX2)
            doseX1 = doseX2;
        String s = String.format(TreatmentDelayText.result,
                fractionPlanned,
                doseFr,
                fractionDelivered,
                delay,
                ab,
                k,
                round(fractionPlanned * doseFr, 2), // planned dose
                round(BEDplanned, 1),
                round(fractionDelivered * doseFr, 2), // delivered dose
                round(BEDdelivered, 1),
                round(BEDrest, 1),
                doseFr,
                round(fractionX, 0),
                fractionRest,
                round(doseX1, 2)
        );
        return s;
    }
    private static double round(double x, int scale){
        BigDecimal y = new BigDecimal(x).setScale(scale, RoundingMode.HALF_UP);
        return y.doubleValue();
    }
    public enum K {
        low(0.1),
        medium(0.3),
        high(0.5);
        public final double value;
        K(double v) {
            this.value = v;
        }
    }
}
