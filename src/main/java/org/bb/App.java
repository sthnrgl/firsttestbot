package org.bb;

import org.bb.controller.Tasks;
import org.bb.settings.Settings;
import org.bb.telegram.LogBot;
import org.bb.telegram.RTbot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

public class App {
    //TODO add email reg + prescription generator
    //TODO add SQL and HTML filter for BD input data: user data, input text
    private static final Logger log = LoggerFactory.getLogger(App.class);
    public static final RTbot rtbot = new RTbot();
    public static final LogBot logbot = new LogBot();
    public static void main(String[] args) {
        Login.getLoginsFromFile("login");
        log.info(Settings.showSettings());
        try {
            TelegramBotsApi registerRTbot = new TelegramBotsApi(DefaultBotSession.class);
            log.info("register bot {}", Login.getBotName1());
            registerRTbot.registerBot(rtbot);
            log.info("{} ready", Login.getBotName1());

            TelegramBotsApi registerLogBot = new TelegramBotsApi(DefaultBotSession.class);
            log.info("register bot {}", Login.getBotName2());
            registerRTbot.registerBot(logbot);
            log.info("{} ready", Login.getBotName2());

            Tasks task1 = new Tasks();
            task1.runBotStatus();


        } catch (TelegramApiException e) {
            log.error("registerFail {}", e.toString());
        }
    }
}
