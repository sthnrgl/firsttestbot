package org.bb.telegram;

import org.bb.App;
import org.bb.Login;
import org.bb.controller.Controller;
import org.bb.controller.Functions;
import org.bb.controller.Message;
import org.bb.settings.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.ForwardMessage;
import org.telegram.telegrambots.meta.api.methods.send.*;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.bb.settings.Settings.*;

public class RTbot extends TelegramLongPollingBot {
    private final Logger logger = LoggerFactory.getLogger(RTbot.class);
    private final boolean forwardUpdatesToAdmin = Settings.getForwardUpdatesToAdmin();
    private static final LogBot logbot = App.logbot;

    @Override
    public String getBotUsername() {
        return Login.getBotName1();
    }
    @Override
    public String getBotToken() {
        return Login.getBotKey1();
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()){
            if (forwardUpdatesToAdmin && !String.valueOf(update.getMessage().getChatId()).equals(Login.getSuperUserID())){
                ForwardMessage forwardMessage =
                        ForwardMessage.builder().
                                messageId(update.getMessage().getMessageId()).
                                chatId(Login.getSuperUserID()).
                                fromChatId(update.getMessage().getChatId()).
                                build();

                try {
                    execute(forwardMessage);
                } catch (TelegramApiException e) {
                    throw new RuntimeException(e);
                }
            }
            {
                send(new Controller().run(update));
            }
        }
    }
    public void send(String id, String message){
        sendText(id, message, "");
    }
    public void send(ArrayList<Message> messages){
        for (Message m : messages){
            switch (m.typeMessage){
                case TEXT -> sendText(m.user.ID, m.text, m.parseMode);
                case MENU -> sendReplayMenu(m.user.ID, Functions.stringToList(m.text), m.parseMode, m.caption);
                case PICTURE -> sendPicture(m.user.ID, m.text);
                case VIDEO -> sendVideo(m.user.ID, m.text);
                case STICKER -> sendSticker(m.user.ID, m.text);
                default -> logger.error("unknown type of message");
            }
        }
    }
    private void sendText(String userID, String message, String parseMode){
        for (String s : Functions.splitText(message)){
            SendMessage sendMessage = SendMessage.builder()
                    .chatId(userID)
                    .text(s).parseMode(parseMode)
                    .build();
            try {
                execute(sendMessage);
            } catch (TelegramApiException e) {
                logger.error(String.valueOf(e));
            }
        }
    }
    private void sendReplayMenu(String userID, List<String> items, String parseMode, String caption){
        if (caption == null)
            caption = DEFAULT_CAPTION;
        int count = 0;
        KeyboardRow keyboardRow = new KeyboardRow();
        Collection<KeyboardRow> collection = new ArrayList<>();
        for (String s : items){
            if (count >= 3) {
                count = 1;
                collection.add(keyboardRow);
                keyboardRow = new KeyboardRow();
            }
            else {
                count++;
            }
            keyboardRow.add(KeyboardButton.builder().text(s).build());
        }
        if (!keyboardRow.isEmpty())
            collection.add(keyboardRow);
        ReplyKeyboardMarkup markup = ReplyKeyboardMarkup
                .builder()
                .clearKeyboard()
                .keyboard(collection)
                .resizeKeyboard(true)
                .oneTimeKeyboard(true)
                .build();
        SendMessage send = SendMessage
                .builder()
                .chatId(userID)
                .replyMarkup(markup)
                .text(caption)
                .parseMode(parseMode)
                .build();

        try {
            execute(send);
        }catch (TelegramApiException e){
            logger.error("Error in sendReplayMenu: ", e);
        }
    }
    private void sendPicture(String userID, String path){
        SendPhoto sendPhoto = SendPhoto
                .builder()
                .chatId(userID)
                .protectContent(false)
                .photo(new InputFile(new File(path)))
                .build();
        try {
            execute(sendPhoto);

        } catch (TelegramApiException e) {
            logger.error("Error send picture: ", e);
        }
    }
    private void sendVideo(String userID, String fileId){
        SendAnimation animation = SendAnimation
                .builder()
                .chatId(userID)
                .animation(new InputFile(new File(fileId)))
                .build();
        try {
            execute(animation);
        } catch (TelegramApiException e) {
            logger.error("Error send animation: ", e);
        }
    }
    private void sendSticker(String userID, String fileID){
        InputFile stickerFile = new InputFile(fileID);
        SendSticker sticker = new SendSticker(userID, stickerFile);
        try {
            execute(sticker);
        } catch (TelegramApiException e) {
            logger.error("Error send  sticker: ", e);
        }

    }
}
