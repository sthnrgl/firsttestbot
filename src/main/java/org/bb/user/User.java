package org.bb.user;

import org.bb.controller.activities.IActivity;
import org.bb.controller.activities.MainMenuActivity;
import org.telegram.telegrambots.meta.api.objects.Update;

public class User {
    public final String ID, nickName, firstName, lastName, languageCode;
    private IActivity activity = new MainMenuActivity();
    private int permissions = 0;
    public User(Update update){
        ID = update.getMessage().getFrom().getId().toString();
        nickName = update.getMessage().getFrom().getUserName();
        firstName = update.getMessage().getFrom().getFirstName();
        lastName = update.getMessage().getFrom().getLastName();
        languageCode = update.getMessage().getFrom().getLanguageCode();
    }
    public User(String ID, String nickName, String firstName, String lastName, String languageCode){
        this.ID = ID;
        this.nickName = nickName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.languageCode = languageCode;
    }

    public void setActivity(IActivity activity){
        this.activity = activity;
    }
    public IActivity getActivity(){
        return this.activity;
    }

    public int getPermissions() {
        return permissions;
    }
    public void setPermissions(int permissions) {
        this.permissions = permissions;
    }
    @Override
    public String toString() {
        return String.format(
                """
                        ID ->\t%s
                        Nick name ->\t%s
                        First name ->\t%s
                        Last name ->\t%s
                        Lang code ->\t%s
                        Permissions ->\t%s
                        Activity -> \t%s
                        
                        """,
                ID, nickName, firstName, lastName, languageCode, permissions, activity
        );
    }
}
