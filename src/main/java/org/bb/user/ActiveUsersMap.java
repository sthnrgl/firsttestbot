package org.bb.user;

import org.bb.db.DB;
import org.bb.constants.UserFieldsDB;
import org.telegram.telegrambots.meta.api.objects.Update;
import java.util.Hashtable;

public class ActiveUsersMap {
    private ActiveUsersMap(){}
    public static final ActiveUsersMap instance = new ActiveUsersMap();
    private final static Hashtable<String, User> userMap = new Hashtable<>( );


    public void addUser(String userID, Update update){
        userMap.put(userID, new User(update));
        //TODO temp option, while BD is incomplete
        addOrSupplementUserData(userMap.get(userID));
    }
    public User getUser(String userID){
        return userMap.get(userID);
    }
    public boolean isUserExist(String userID){
        return userMap.containsKey(userID);
    }
    private void addOrSupplementUserData(User user){
        if (!DB.isUserExist(user.ID))
            DB.addUser(user);
        else {
            //User userbd = BDcom.getUser(user.ID);
            DB.updateUserField(user.ID,UserFieldsDB.nickname, user.nickName);
            DB.updateUserField(user.ID, UserFieldsDB.firstname, user.firstName);
            DB.updateUserField(user.ID, UserFieldsDB.lastname, user.lastName);
            DB.updateUserField(user.ID, UserFieldsDB.languagecode, user.languageCode);
        }
    }
    public static String getListOfActiveUsers(){
        StringBuilder sb = new StringBuilder();
        for (String key : userMap.keySet()){
            User u = ActiveUsersMap.instance.getUser(key);
            sb.append(String.format("\n%s\n", u.toString()));
        }
        return sb.toString();
    }
    public static String getNumberOfActiveUsers(){
        return String.valueOf(userMap.size());
    }
}
