package org.bb.db;

import org.bb.constants.UserFieldsDB;
import org.bb.constants.doseconstraints.Sites;
import org.bb.controller.Functions;
import org.bb.user.User;
import org.bb.constants.doseconstraints.Fractions;
import java.sql.*;
import java.util.ArrayList;

public class DB {
    private static final String
            url = "jdbc:mariadb://localhost:3306/RTbot",
            user = "root",
            pass = "a";

    private static final Connection connection;

    static {
        try {
            connection = DriverManager.getConnection(url, user, pass);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private static String validator(String s){
        if (s == null || s.isEmpty())
            return s;
        char[] ch = {'&','<','>','/','\\','"','"','?','+'};
        for (char ch2 : ch)
            s = s.replace(ch2, ' ');
        return s;
    }
    public static boolean isUserExist(String ID){
        ID = validator(ID);
        String SELECT = String.format("SELECT * FROM user WHERE id = '%s'", ID);
        try {
            PreparedStatement st = connection.prepareStatement(SELECT);
            ResultSet result = st.executeQuery();
            st.close();
            return result.next();

        } catch (SQLException e) {
            System.out.println("cannot create user");
            return false;
        }
    }
    public static boolean addUser(User user){
        String ID = validator(user.ID),
                nickName = validator(user.nickName),
                firstName = validator(user.firstName),
                lastName = validator(user.lastName),
                languageCode = validator(user.languageCode);
        try {
            String INSERT_USER = String.format("INSERT INTO user(id, nickname, firstname, lastname, languagecode) " +
                            "VALUES('%s','%s','%s','%s','%s');",
                    ID, nickName, firstName, lastName, languageCode);
            PreparedStatement st = connection.prepareStatement(INSERT_USER);
            st.executeUpdate();
            st.close();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    public static User getUser(String ID){
        try {
            ID = validator(ID);
            User user;
            String SELECT = String.format("SELECT * FROM user WHERE id = '%s'", ID);
            PreparedStatement st = connection.prepareStatement(SELECT);
            ResultSet result = st.executeQuery();
            result.next();
            user = new User(
                    result.getString("id"),
                    result.getString("nickname"),
                    result.getString("firstname"),
                    result.getString("lastname"),
                    result.getString("languagecode")
            );
            st.close();
            return user;
        } catch (SQLException e) {
            return null;
        }
    }

    public static boolean updateUserField(String ID,UserFieldsDB field, String newValue){
        try {
            ID = validator(ID);
            newValue = validator(newValue);
            String INSERT = String.format(
                    """
                            Update user
                            SET %s = '%s'
                            WHERE id = '%s';
                            """,
                    field,
                    newValue,
                    ID
            );
            PreparedStatement insert = connection.prepareStatement(INSERT);
            insert.executeUpdate();
            insert.close();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    public static String numberOfUsers(){
        String SELECT = "SELECT COUNT(*) FROM user;";
        try {
            PreparedStatement st = connection.prepareStatement(SELECT);
            ResultSet result = st.executeQuery();
            result.next();
            String number = String.valueOf(result.getInt(1));
            st.close();
            return number;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public static String getSettings(String id){
        try {
            id = validator(id);
            String SELECT = String.format("SELECT * FROM settings WHERE id = '%s';", id);
            PreparedStatement st = connection.prepareStatement(SELECT);
            ResultSet result = st.executeQuery();
            result.next();
            String value = result.getString("value");
            st.close();
            return value;
        } catch (SQLException e) {
            return null;
        }
    }
    public static boolean setSettings(String id, String value){
        try {
            id = validator(id);
            value = validator(value);
            String INSERT = String.format(
                    "UPDATE settings " +
                            "SET value='%s' " +
                            "WHERE id='%s';",
                    value, id);
            PreparedStatement st = connection.prepareStatement(INSERT);
            st.executeUpdate();
            st.close();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }
    public static String getOrgan(String organ, Fractions fraction){
        organ = validator(organ);
        String SELECT = String.format(
                "SELECT %s FROM dose_constraints WHERE organ='%s';",
                fraction.name(), organ);
        try {
            PreparedStatement st = connection.prepareStatement(SELECT);
            ResultSet result = st.executeQuery();
            result.next();
            String value = result.getString(fraction.name());
            st.close();
            return Functions.safeHTML(value);
        } catch (SQLException e) {
        return null;
        }
    }

    public static String filterFractionSite(Fractions fraction, Sites site){
        String SELECT = String.format(
                "SELECT organ FROM dose_constraints WHERE (%s=true AND %s IS NOT NULL);",
                site, fraction.name());
        try {
            StringBuilder sb = new StringBuilder();
            PreparedStatement st = connection.prepareStatement(SELECT);
            ResultSet result = st.executeQuery();
            while (result.next()){
                sb.append(result.getString("organ")).append(",");
            }
            String s = sb.toString();
            st.close();
            if (s.isEmpty())
                return Functions.safeHTML(s);
            else
                return s.substring(0, s.length() - 1);
        } catch (SQLException e) {
            return "";
        }
    }

    public static ArrayList<String> getAllOrgans(){
        ArrayList<String> list = new ArrayList<>();
        String SELECT = "SELECT organ FROM dose_constraints;";
        try {
            //StringBuilder sb = new StringBuilder();
            PreparedStatement st = connection.prepareStatement(SELECT);
            ResultSet result = st.executeQuery();
            while (result.next()){
                list.add(result.getString("organ"));
            }
        } catch (SQLException e) {
            list.add("ERROR: failed to read the database");
        }
        return list;
    }


}
