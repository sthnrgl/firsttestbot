package org.bb.apps;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.bb.apps.BED.calculate;
import static org.bb.constants.text.BEDtext.*;
import static org.junit.jupiter.api.Assertions.*;

class BEDTest {

    @ParameterizedTest(name = "BED args {index}")
    @ValueSource(strings = {"", "1", "1 1 1 1", "word123"})
    void incorrectNumberArguments(String s){
        assertEquals(INCORRECT_NUMBER_ARGS.value, calculate(s));
    }

    @Test
    void isNull(){
        assertEquals(INCORRECT_NUMBER_ARGS.value, calculate(null));
    }

    @ParameterizedTest(name = "BED frac {index}")
    @ValueSource(strings = {"7,2,2", "41 140 2", "0 1 1 ", "0 2 2"})
    void incorrectFractions(String s){
        assertEquals(String.format(INVALID_FRAC.value, BED.minFrac, BED.maxFrac), calculate(s));
    }
}