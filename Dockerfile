FROM ubuntu:latest

COPY target/RTbot2.2-2.2-jar-with-dependencies.jar /bot.jar
COPY login /login
COPY res /res
COPY dump.sql /dump.sql
COPY startd.sh /startd.sh

RUN apt update
RUN apt install -y openjdk-17-jdk-headless
RUN apt install -y mariadb-server
RUN chmod +x /startd.sh

CMD ./startd.sh

